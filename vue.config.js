module.exports = {
    devServer: {
        proxy: {
            '^/api/.*': {
                target: "http://localhost:8000",
                changeOrigin: true,
                secure: false
            }
        }
    },
    lintOnSave: process.env.NODE_ENV !== 'production'
};
