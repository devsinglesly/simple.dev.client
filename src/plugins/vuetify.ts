import Vue from 'vue';
// @ts-ignore
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#2962ff',
    secondary: '#212121',
    accent: '#0091ea',
    error: '#d50000',
    info: '#00e5ff',
    success: '#00c853',
    warning: '#ff6d00',
  },
});
