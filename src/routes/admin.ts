import {RouteConfig} from 'vue-router';
import {store} from '@/store';
import {CategoriesQueryParams} from '@/services/backend/Categories';
import {LessonsQueryParams} from '@/services/backend/Lessons';

export const prefix: string = 'admin_';

const routes: RouteConfig[] = [
  {
    path: '/',
    redirect: '/admin/categories',
  },
  {
    path: '/admin/categories',
    name: prefix + 'categories',
    component: () => import(/* webpackChunkName: "admin.categories" */ '@/views/admin/Categories.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('categories/getAll');
      next();
    },
  },
  {
    path: '/admin/categories/:category_id/lessons',
    name: prefix + 'categories_lessons',
    component: () => import(/* webpackChunkName: "admin.lessons" */ '@/views/admin/Lessons.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('categories/getAll');
      await store.dispatch('lessons/getAll', {category: to.params.category_id} as LessonsQueryParams);
      next();
    },
  },
  {
    path: '/admin/lessons',
    name: prefix + 'lessons',
    component: () => import(/* webpackChunkName: "admin.lessons" */ '@/views/admin/Lessons.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('categories/getAll');
      await store.dispatch('lessons/getAll');
      next();
    },
  },
  {
    path: '/lesson/:id',
    name: prefix + 'simple_lesson',
    component: () => import(/* webpackChunkName: "admin.lessons" */ '@/views/admin/Lessons.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('categories/load', { publicate: 'all' } as CategoriesQueryParams);
      await store.dispatch('lessons/load', { publicate: 'all' } as LessonsQueryParams);
      next();
    },
  },
  {
    path: '/admin/users',
    name: prefix + 'users',
    component: () => import(/* webpackChunkName: "admin.users" */ '@/views/admin/Users.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('users/getAll');
      next();
    },
  },
  {
    path: '/admin/courses',
    name: prefix + 'courses',
    component: () => import(/* webpackChunkName: "admin.courses" */ '@/views/admin/courses/Courses.vue'),
  },
  {
    path: '/admin/bids',
    name: prefix + 'bids',
    component: () => import(/* webpackChunkName: "admin.bids" */ '@/views/admin/Bids.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('bids/getNew');
      next();
    },
  },
];



export default routes;
