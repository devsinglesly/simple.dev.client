import {RouteConfig} from 'vue-router';
import {store} from '@/store';
import {LessonsQueryParams} from '@/services/backend/Lessons';

export const prefix: string = 'main_';

const routes: RouteConfig[] = [
  {
    path: '',
    redirect: '/categories',
  },
  {
    path: '/preview',
    name: prefix + 'preview',
    component: () => import(/* webpackChunkName: "main.preview" */ '@/views/main/Preview.vue'),
  },
  {
    path: '/categories',
    name: prefix + 'categories',
    component: () => import(/* webpackChunkName: "main.categories" */ '@/views/main/Categories.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('categories/loadPublicate');
      next();
    },
  },
  {
    path: '/categories/:category_id/lessons',
    name: prefix + 'categories_lessons',
    component: () => import(/* webpackChunkName: "main.categories.lessons" */ '@/views/main/Lessons.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('lessons/loadPublicate', { category: to.params.category_id} as LessonsQueryParams );
      next();
    },
  },
  {
    path: '/lessons',
    name: prefix + 'lessons',
    component: () => import(/* webpackChunkName: "main.lessons" */ '@/views/main/Lessons.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('lessons/loadPublicate');
      next();
    },
  },
  {
    path: '/simple/lesson/:id',
    name: prefix + 'simple_lesson',
    component: () => import(/* webpackChunkName: "main.lessons" */ '@/views/main/SimpleLesson.vue'),
    async beforeEnter(to: any, from: any, next: any) {
      await store.dispatch('lessons/fetchById', to.params.id);
      next();
    },
  },
];

export default routes;
