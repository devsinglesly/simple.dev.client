import Basic from './Basic';

export interface FileUploadResponseInterface {
  hash: string;
}

export class File extends Basic {
  protected prefix: string = '/file';


  public async upload(uploadFile: any) {
    const fd = new FormData();
    fd.append('file', uploadFile);
    return (await this.post('/upload', fd)).hash;
  }
}

const file = new File();

export default file;
