import Basic from './Basic';
import {UsersType} from './Users';


export class Auth extends Basic {

  constructor() {
    super();
    this.prefix = '/users';
  }

  public async signup(user: UsersType): Promise<UsersType> {
    const res = (await this.post('/signup', user));

    if (res.access_token) {
      window.localStorage.setItem('access_token', res.access_token);
    }

    return res.user_info;
  }

  public async check() {
    try {
      return await this.get('/check');
    } catch (e) {
      return null;
    }
  }

  public async login(user: UsersType): Promise<UsersType> {
    const res = await this.post(`/auth`, user);

    if (res.access_token) {
      window.localStorage.setItem('access_token', res.access_token);
    }

    return res.user_info;
  }

  public async info() {
    const token =  window.localStorage.getItem('token');
    if (!token) {
      throw new Error('token dont find in local storage');
    }
    return await this.get(`/info`);
  }

  public logout = async (): Promise<void> => {
    window.localStorage.removeItem('access_token');
  }

}
export default new Auth();

