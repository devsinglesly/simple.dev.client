import Basic from '@/services/backend/Basic';

export enum BidStatus {
  Accept,
  Reject,
  Wait,
}

export interface Bid {
  id: number;
  last_name: string;
  first_name: string;
  age: string;
  email: string;
  phone: string;
  info: string;
  status: string;
  callStatus?: BidStatus;
}

export interface BidsQueryParams {
  order?: 'ASC' | 'DESC';
  limit?: number;
  offset?: number;
  status?: BidStatus;
}

export class Form extends Basic {

  constructor() {
    super();
    this.prefix = '/bids';
  }

  public async send(form: Bid): Promise<Bid> {
    return await this.post('/', form) as Bid;
  }

  public async getAll(params: BidsQueryParams): Promise<Bid[]> {
    return await this.get('/', {params}) as Bid[];
  }
}

export default new Form();
