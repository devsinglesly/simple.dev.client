import Categories, {CategoriesType} from '@/services/backend/Categories';
import Lessons, {LessonsType} from '@/services/backend/Lessons';
import {UsersType} from '@/services/backend/Users';

export class Lesson implements LessonsType {

  public title: string;
  public description: string;
  public category: CategoriesType | number | null;
  public youtube: string;
  public id?: number;
  public preview?: string;
  public published?: boolean;
  public repository?: string;
  public teacher?: UsersType;

  public constructor(
    title: string = '',
    description: string = '',
    youtube: string = '',
    repository: string = '',
    id: number = null,
    preview: string = null,
    published: boolean = false,
    teacher: UsersType = null,
    category: CategoriesType | number= null,
  ) {
    this.id = id;
    this.description = description;
    this.title = title;
    this.preview = preview;
    this.teacher = teacher;
    this.repository = repository;
    this.youtube = youtube;
    this.published = published;
    this.category = category;
  }

  public changePublicate?: () => Promise<LessonsType> = async (): Promise<LessonsType> => {
    const lesson = await Lessons.changePublicate(this.id);
    this.published = lesson.published as boolean;
    return this;
  }

  public update?: () => Promise<LessonsType> = async (): Promise<LessonsType> => {
    await Lessons.update(this);
    return this;
  }

  public save?: () => Promise<LessonsType> = async (): Promise<LessonsType> => {
    return await Lessons.add(this);
  }

  public changeImage?: (file: any) => Promise<LessonsType> = async (file: any): Promise<LessonsType> => {
    const lesson = await Lessons.changeImage(this.id, file);
    this.preview = lesson.preview;
    return this;
  }

  public remove?: () => Promise<number> = async (): Promise<number> => {
    await Lessons.remove(this);
    return this.id;
  }
}
