import Categories, {CategoriesType} from '@/services/backend/Categories';

export class Category implements CategoriesType {
  public description: string;
  public id?: number;
  public preview?: string;
  public published?: boolean;
  public title: string;

  public constructor(
    title: string,
    description: string,
    id: number = null,
    preview: string = null,
    published: boolean = false,
  ) {
    this.id = id;
    this.description = description;
    this.title = title;
    this.preview = preview;
    this.published = published;
  }

  public changePublicate?: () => Promise<CategoriesType> = async (): Promise<CategoriesType> => {
    const category = await Categories.changePublicate(this.id);
    this.published = category.published as boolean;
    return this;
  }

  public update?: () => Promise<CategoriesType> = async (): Promise<CategoriesType> => {
    await Categories.update(this);
    return this;
  }

  public save?: () => Promise<CategoriesType> = async (): Promise<CategoriesType> => {
    return await Categories.add(this);
  }

  public changeImage?: (file: any) => Promise<CategoriesType> = async (file: any): Promise<CategoriesType> => {
    const category = await Categories.changeImage(this.id, file);
    this.preview = category.preview;
    return this;
  }

  public remove?: () => Promise<number> = async (): Promise<number> => {
    await Categories.remove(this);
    return this.id;
  }

}
