import {Address, Contact, UsersPreviligesEnum, UsersType} from '@/services/backend/Users';

export class User implements UsersType {

  public id: number;
  public age: number | null;
  public type: UsersPreviligesEnum;
  public firstName: string | null;
  public lastName: string | null;
  public middleName: string | null;
  public email: string;
  public avatar: string | null;
  public contact: Contact;
  public address: Address;

  get fullName() {
    const last = this.lastName ? this.lastName : '';
    const first = this.firstName ? this.firstName : '';
    const middle = this.middleName ? this.middleName : '';
    if (last === '' && first === '' && middle === '') {
      return 'Не указано';
    }
    return `${last} ${first} ${middle}`;
  }

  get previliges() {
    switch (this.type) {
      case UsersPreviligesEnum.Default:
        return 'Стандарт';
      case UsersPreviligesEnum.Teacher:
        return 'Учитель';
      case UsersPreviligesEnum.Student:
        return 'Ученик';
      case UsersPreviligesEnum.Admin:
        return 'Админ';
    }
  }
}
