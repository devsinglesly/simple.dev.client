import axios, {AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse} from 'axios';

export default abstract class Basic {


  protected static async requestSuccess(config: AxiosRequestConfig) {

    const token = window.localStorage.getItem('access_token');
    if (token) {
      config.headers.HTTP_Authorization = token;
    }

    return config;
  }

  protected prefix: string = '';

  private axios: AxiosInstance;

  private config: AxiosRequestConfig = {
    baseURL: process.env.VUE_APP_SERVER_URL + '/api',
  };



  constructor() {
    this.axios = axios.create(this.config);
    this.axios.interceptors.request.use(Basic.requestSuccess);
    this.axios.interceptors.response.use((res: AxiosResponse) => res,
      (error: AxiosError) => {
        if (error.response.data.error) {
          return Promise.reject(error.response.data.error);
        }
        return Promise.reject(error);
    });
  }

  protected async get(url: string, config: AxiosRequestConfig = {} ) {

    const response: AxiosResponse = await this.axios.get(this.prefix + url, config);

    if (response.data.status && response.data.status === false) {
      throw new Error(response.data.errors);
    }

    if (response.status !== 200) {
      throw new Error(response.statusText);
    }

    return response.data;

  }

  protected async post(url: string, data: object | FormData, config: AxiosRequestConfig = {}) {

    const response: AxiosResponse = await this.axios.post(this.prefix + url, data, config);

    if (response.data.status && response.data.status === false) {
      throw new Error(response.data.errors);
    }

    if (response.status !== 200) {
      throw new Error(response.statusText);
    }

    return response.data;

  }

  protected async put(url: string, data: object | FormData, config: AxiosRequestConfig = {}) {

    const response: AxiosResponse = await this.axios.put(this.prefix + url, data, config);

    if (response.data.status && response.data.status === false) {
      throw new Error(response.data.errors);
    }

    if (response.status !== 200) {
      throw new Error(response.statusText);
    }

    return response.data;

  }

  protected async patch(url: string, data: object | FormData, config: AxiosRequestConfig = {}) {

    const response: AxiosResponse = await this.axios.patch(this.prefix + url, data, config);

    if (response.data.status && response.data.status === false) {
      throw new Error(response.data.errors);
    }

    if (response.status !== 200) {
      throw new Error(response.statusText);
    }

    return response.data;

  }

  protected async delete(url: string, config: AxiosRequestConfig = {}) {

    const response: AxiosResponse = await this.axios.delete(this.prefix + url, config);

    if (response.data.status && response.data.status === false) {
      throw new Error(response.data.errors);
    }

    if (response.status !== 200) {
      throw new Error(response.statusText);
    }

    return response.data;

  }
}
