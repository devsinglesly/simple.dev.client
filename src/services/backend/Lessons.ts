import Basic from './Basic';
import {UsersType} from './Users';
import {CategoriesType} from './Categories';
import File from '@/services/backend/File';
import {AxiosRequestConfig} from 'axios';
import {Lesson} from '@/services/backend/models/Lesson';

export interface LessonsQueryParams {
    offset?: number;
    limit?: number;
    order?: 'DESC' | 'ASC';
    category?: number;
}

export interface LessonsType {
    id?: number;
    title: string;
    description: string;
    repository?: string;
    youtube: string;
    preview?: string;
    published?: boolean;
    category?: CategoriesType| number | null;
    teacher?: UsersType;
}


export class Lessons extends Basic {
    public static arrayToLesson(item: LessonsType): Lesson {
        return new Lesson(
          item.title, item.description, item.youtube, item.repository,
          item.id, item.preview, item.published, item.teacher, item.category,
        );
    }

    constructor() {
        super();
        this.prefix = '/learning/lesson';
    }

    public async loadPublicate(params: LessonsQueryParams = {}): Promise<LessonsType[]> {
        const conf: AxiosRequestConfig = {params};
        const items = await this.get('/published', conf);
        return items.map((item: LessonsType) => {
            return Lessons.arrayToLesson(item);
        });
    }

    public async getAll(params: LessonsQueryParams = {}): Promise<LessonsType[]> {
        const conf: AxiosRequestConfig = { params };
        const items = await this.get('/', conf);
        return items.map((item: LessonsType) => {
            return Lessons.arrayToLesson(item);
        });
    }

    public async getById(id: number): Promise<LessonsType> {
        const item = await this.get(`/${id}`);
        return Lessons.arrayToLesson(item);
    }

    public async changePublicate(id: number): Promise<LessonsType> {
        const item = await this.patch(`/${id}/publish`, {});
        return Lessons.arrayToLesson(item);
    }

    public async add(lesson: LessonsType): Promise<LessonsType> {
        const item = await this.post('/', lesson);
        return Lessons.arrayToLesson(item);
    }

    public async update(lesson: LessonsType) {
        const id: number = lesson.id as number;
        lesson.category = typeof lesson.category === 'object' ? lesson.category.id : lesson.category as number;
        const item = await this.put(`/${id}`, lesson);
        return Lessons.arrayToLesson(item);
    }

    public async changeImage(id: number, file: File) {
        const hash = await File.upload(file);
        const item = await this.patch(`/${id}/image`, {path : hash});
        return Lessons.arrayToLesson(item);
    }

    public async remove(lesson: LessonsType) {
        const id: number = lesson.id as number;

        if (id === undefined) {
            throw new Error('id of topic is undefined');
        }

        return await this.delete(`/${id}`);
    }

    public async removeById(id: number) {

        if (id === undefined) {
            throw new Error('id is undefined');
        }

        return await this.delete(`/${id}`);
    }

}

export default new Lessons();
