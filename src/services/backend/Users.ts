import Basic from './Basic';
import {User} from '@/services/backend/models/User';
import {Bid} from '@/services/backend/bids/Form';

export enum UsersPreviligesEnum {
  Default = 0,
  Student = 1,
  Teacher = 2,
  Admin   = 3,
}

export interface UsersQueryParams {
  order?: 'DESC' | 'ASC';
  limit?: number;
  offset?: number;
  active?: boolean;
  type?: UsersPreviligesEnum;
}

export interface Contact {
  discord?: string;
  skype?: string | null;
  vk?: string | null;
}

export interface Address {
  build?: string | null;
  country?: string | null;
  city?: string | null;
  flat?: string | null;
  street?: string | null;
}

export interface UsersType {
  id?: number;
  age?: number;
  type?: UsersPreviligesEnum;
  email?: string;
  avatar?: string;
  password?: string;
  lastName?: string;
  firstName?: string;
  middleName?: string;
  contact?: Contact;
  address?: Address;
}

export class Users extends Basic {

  public static convert(user: UsersType): User {
    const u = new User();
    u.id = user.id;
    u.lastName = user.lastName;
    u.firstName = user.firstName;
    u.middleName = user.middleName;
    u.email = user.email;
    u.address = user.address;
    u.contact = user.contact;
    u.age = user.age;
    u.type = user.type;
    return u;
  }

  constructor() {
    super();
    this.prefix = '/users';
  }

  public async getAll(params: UsersQueryParams): Promise<User[]> {
    const users = await this.get('/', { params });
    return users.map((item) => Users.convert(item));
  }

  public async create(user: UsersType) {
    return await this.post('', user);
  }

  public async update(user: UsersType) {
    const id: number = user.id as number;

    if (id === undefined) {
      throw new Error('user id is undefined');
    }

    return await this.put(`/${id}`, user);
  }

  public async acceptBid(bid: Bid, price: number) {
    if (price <= 0) {
      throw new Error('Стоимость не может быть меньше нуля');
    }
    return await this.post(`/bid/create/${bid.id}`, {price});
  }

  public async sendPassword(user: User) {
    return await this.post(`/send/password`, {uid: user.id});
  }
}
export default new Users();
