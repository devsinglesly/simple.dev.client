import Basic from './Basic';
import { AxiosRequestConfig } from 'axios';
import File from '@/services/backend/File';
import {Category} from '@/services/backend/models/Category';

export interface CategoriesType {
  id?: number;
  title: string;
  description: string;
  preview?: string;
  published?: boolean;
}

export interface CategoriesQueryParams {
  offset?: number;
  orderBy?: string;
  limit?: number;
}

export class Categories extends Basic {

  public static arrayToCategory(item: CategoriesType): Category {
    return new Category(item.title, item.description, item.id, item.preview, item.published);
  }

  constructor() {
    super();
    this.prefix = '/learning/category';
  }


  public async getAll(params: CategoriesQueryParams = {}): Promise<Category[]> {
    const conf: AxiosRequestConfig = { params };
    const items = await this.get('/', conf);
    return items.map((item: CategoriesType) => {
      return Categories.arrayToCategory(item);
    });
  }

  public async getPublished(params: CategoriesQueryParams = {}): Promise<CategoriesType[]> {
    const conf: AxiosRequestConfig = { params };
    const items = await this.get('/published', conf);
    return items.map((item: CategoriesType) => {
      return Categories.arrayToCategory(item);
    });
  }


  public async changePublicate(id: number): Promise<CategoriesType> {
    const item = (await this.patch(`/${id}/publish`, {}));
    return Categories.arrayToCategory(item);
  }

  public async add(category: CategoriesType): Promise<CategoriesType> {
    const item = await this.post('/', category);
    return Categories.arrayToCategory(item);
  }

  public async update(category: CategoriesType) {
    const id: number = category.id as number;
    const item = (await this.put(`/${id}`, category));
    return Categories.arrayToCategory(item);
  }

  public async changeImage(id: number, file: File) {
    const hash = await File.upload(file);
    const item = await this.patch(`/${id}/image`, {path: hash});
    return Categories.arrayToCategory(item);
  }

  public async remove(category: CategoriesType) {
    const id: number = category.id as number;

    if (id === undefined) {
      throw new Error('id of topic is undefined');
    }

    return await this.delete(`/${id}`);
  }

  public async removeById(id: number) {

    if (id === undefined) {
      throw new Error('id is undefined');
    }

    return await this.delete(`/${id}`);
  }
}

const categories = new Categories();
export default categories;
