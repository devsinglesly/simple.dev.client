import Vue from 'vue';
import Router, {RouteConfig, RouterMode, RouterOptions} from 'vue-router';

import admin, {prefix} from './routes/admin';
import main from './routes/main';
import dashboard from './routes/dashboard';
import {store} from '@/store';

Vue.use(Router);

const mode: RouterMode = 'history';

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "main" */ '@/layouts/Main.vue'),
    children: [
      ...main,
    ],
  },
  {
    path: '/signin',
    name: 'signin',
    component: () => import(/* webpackChunkName: "signin" */ '@/views/Signin.vue'),
    meta: {
      requiredGuest: true,
    },
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => import(/* webpackChunkName: "signup" */ '@/views/Signup.vue'),
    meta: {
      requiredGuest: true,
    },
  },
  {
    path: '/admin',
    component: () => import(/* webpackChunkName: "admin" */ '@/layouts/Admin.vue'),
    children: [
      ...admin,
    ],
    meta: {
      requiredAdmin: true,
    },
  },
  {
    path: '/dashboard',
    children: [
      ...dashboard,
    ],
    meta: {
      requiredTeacher: true,
      requiredAdmin: true,
    },
  },
];

const options: RouterOptions = {
  mode,
  routes,
};

const router = new Router(options);

const AuthChecked = async () => {
  await store.dispatch('auth/check');
  return store.getters['auth/isAuth'];
};

router.beforeEach(async (to: any, from: any, next: any): Promise<void> => {
    if (to.matched.some((record) => record.meta.requiredAdmin)) {
      await AuthChecked();
      const isAdmin = store.getters['auth/isAdmin'];
      if (isAdmin) {
        next();
      } else {
        next(from);
      }
      return;
    }

    next();
});



export default router;
