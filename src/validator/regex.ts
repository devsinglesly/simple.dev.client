export const minValue = 4;

export const required = (v: string) => v.length >= 1 || 'Заполните поле';
// @ts-ignore
export const email = (v: string) => /.+@.+\..+/.test(v) || 'Не корректный email';

export const min = (v: string) => v.length > minValue || `Больше ${minValue} символов`;

export const phone = (v: string) => {
  return /^\+\d\s\(\d{3}\)\s\d{3}\s\d{2}-\d{2}$/.test(v) || 'Не корректный номер';
};

export default {
  required,
  email,
  min,
  phone,
  settings: {
    minValue,
  },
};
