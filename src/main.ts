import Vue from 'vue';
import './plugins/vuetify';
import './plugins/axios';
import App from './App.vue';
import router from './router';
import {store} from './store';
import './styles/main.less';

import OverlayDisabledScreenComponent from '@/components/overlays/OverlayDisabledScreen.vue';

Vue.config.productionTip = false;

Vue.component('app-overlay-disabled-screen', OverlayDisabledScreenComponent);


try {
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount('#app');
} catch (e) {
  console.log(e);
}
