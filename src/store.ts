import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import {auth} from '@/store/auth';
import {categories} from '@/store/categories';
import {lessons} from '@/store/lessons';
import {bids} from '@/store/bids';
import {users} from '@/store/users';


export const store = new Vuex.Store({
  state : {
    lang: 'ru',
  },
  getters : {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    categories,
    lessons,
    bids,
    users,
  },
});
