import {ActionTree, GetterTree, MutationTree} from 'vuex';

import {AuthState as State} from './types';

import Auth from '@/services/backend/Auth';
import {UsersPreviligesEnum, UsersType} from '@/services/backend/Users';
import router from '@/router';

const state: State = {
  user: null,
};

const getters: GetterTree<State, any> = {
  isAuth: (store: State) => store.user !== null,
  isDefault: (store: State) => {
    if (!store.user) { return false; }
    return store.user.type === UsersPreviligesEnum.Default;
  },
  isAdmin: (store: State) => {
    if (!store.user) { return false; }
    return store.user.type === UsersPreviligesEnum.Admin;
  },
  isStudent: (store: State) => {
    if (!store.user) { return false; }
    return store.user.type === UsersPreviligesEnum.Student;
  },
  isTeacher: (store: State) => {
    if (!store.user) { return false; }
    return store.user.type === UsersPreviligesEnum.Teacher;
  },
};

const mutations: MutationTree<State> = {
  user: (store, payload: UsersType) => (store.user = payload),
};

const actions: ActionTree<State, any> = {
  async signup(store, payload: UsersType) {
    const user = await Auth.signup(payload);
    store.commit('user', user);
  },
  async login(store, payload: UsersType) {
    const user = await Auth.login(payload);
    store.commit('user', user);
  },
  async check(store): Promise<UsersType> {
    const user = await Auth.check();
    store.commit('user', user);
    return user;
  },
  async logout(store) {
    await Auth.logout();
    store.commit('user', null);
    router.push({name: 'home'});
  },
};

export const auth = {
  namespaced: true,
  actions,
  mutations,
  getters,
  state,
};
