import {GetterTree, ActionTree, MutationTree, Module} from 'vuex';

import {CategoriesState as State} from '@/store/types';
import Categories, {CategoriesQueryParams, CategoriesType} from '@/services/backend/Categories';
import {Category} from '@/services/backend/models/Category';

const state: State = {
  items: [],
};

const getters: GetterTree<State, any> = {
  items: (store) => store.items,
  map: (store) => {
    const map = {};
    store.items.forEach((item, i) => {
      map[item.id] = i;
    });
    return map;
  },
};

const mutations: MutationTree<State> = {
  items: (store, payload: CategoriesType[]) => store.items = payload,
  addToBegin: (store, item: Category) =>  store.items.unshift(item),
  addToEnd: (store, item: Category) => store.items.push(item),
  removeById: (store, idx: number) => {
    store.items.splice(idx, 1);
  },
};

const actions: ActionTree<State, any> = {
  async loadPublicate(store, params: CategoriesQueryParams) {
    store.commit('items', await Categories.getPublished(params));
    return;
  },
  async getAll(store, params: CategoriesQueryParams) {
    const items = await Categories.getAll(params);
    store.commit('items', items);
    return;
  },
  async removeById(store, id: number) {
    store.commit('removeById', store.getters.map[id]);
  },
};

export const categories: Module<State, any> = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
