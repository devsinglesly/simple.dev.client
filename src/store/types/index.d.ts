/** Sections state module */
import {UsersType} from '@/services/backend/Users';
import {CategoriesType} from '@/services/backend/Categories';
import {LessonsType} from '@/services/backend/Lessons';



export interface CategoriesState {
  items: CategoriesType[];
}


/** Auth state module */
export interface AuthState {
  user: UsersType | null;
}

export interface LessonsState {
  items: LessonsType[];
  is_loaded: boolean;
}

/** Users state module */
export interface UsersState {
  items: UsersType[];
}
