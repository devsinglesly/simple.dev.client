import {ActionTree, GetterTree, MutationTree} from 'vuex';
import Form, {Bid, BidsQueryParams, BidStatus} from '@/services/backend/bids/Form';

interface State {
  items: Bid[];
}

const state: State = {
  items: [],
};

const getters: GetterTree<State, any> = {
  items: (store) => store.items,
};

const mutations: MutationTree<State> = {
  items: (store, payload: Bid[]) => store.items = payload,
};

const actions: ActionTree<State, any> = {
  async getNew(store, params: BidsQueryParams = {}): Promise<void> {
    params.status = BidStatus.Wait;
    const items = await Form.getAll(params);
    store.commit('items', items);
    return;
  },
  async refresh(store) {
    const items = await Form.getAll({
      status: BidStatus.Wait,
    });
    store.commit('items', items);
    return;
  },
  async getAccept(store, params: BidsQueryParams = {}): Promise<void> {
    params.status = BidStatus.Accept;
    const items = await Form.getAll(params);
    store.commit('items', items);
    return;
  },
  async getReject(store, params: BidsQueryParams = {}): Promise<void> {
    params.status = BidStatus.Reject;
    const items = await Form.getAll(params);
    store.commit('items', items);
    return;
  },
};

export const bids = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
