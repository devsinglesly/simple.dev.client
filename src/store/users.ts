import {GetterTree, ActionTree, MutationTree} from 'vuex';
import {User} from '@/services/backend/models/User';
import {UsersQueryParams} from '@/services/backend/Users';
import Users from '@/services/backend/Users';

interface State {
  items: User[];
}


const state: State = {
  items: [],
};

const getters: GetterTree<State, any> = {
  items: (store) => store.items,
};

const mutations: MutationTree<State> = {
  items: (store, payload: User[]) => store.items = payload,
};

const actions: ActionTree<State, any> = {
  async getAll(store, params: UsersQueryParams = {}): Promise<User[]> {
    const items = await Users.getAll(params);
    store.commit('items', items);
    return items;
  },
};

export const users = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
