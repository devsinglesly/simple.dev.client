import {GetterTree, ActionTree, MutationTree} from 'vuex';

import {LessonsState as State} from '@/store/types';
import Lessons, {LessonsQueryParams, LessonsType} from '@/services/backend/Lessons';
import {Lesson} from '@/services/backend/models/Lesson';

const state: State = {
  items: [],
  is_loaded: false,
};

const getters: GetterTree<State, any> = {
  items: (store) => store.items,
  is_loaded: (store): boolean => store.is_loaded,
  map: (store) => {
    const map = {};
    store.items.forEach((item, i) => {
      map[item.id] = i;
    });
    return map;
  },
  getById: (store, gtr) => (id: number) => {
    return store.items[gtr.map[id]];
  },
};

const mutations: MutationTree<State> = {
  items: (store, payload: LessonsType[]) => store.items = payload,
  addToBegin: (store, item: LessonsType) =>  store.items.unshift(item),
  addToEnd: (store, item: LessonsType) => store.items.push(item),
  removeById: (store, id: number) => {
    store.items.splice(getters.map[id], 1);
  },
  is_loaded: (store, value: boolean) => store.is_loaded = value,
};

const actions: ActionTree<State, any> = {
  loadPublicate: async (store, params: LessonsQueryParams) => {
    store.commit('items', await Lessons.loadPublicate(params));
    store.commit('is_loaded', true);
  },
  fetchById: async (store, id: number) => {
    const item = await Lessons.getById(id);
    store.commit('addToEnd', item);
    return;
  },
  getAll: async (store, params: LessonsQueryParams) => {
    store.commit('items', await Lessons.getAll(params));
    return;
  },
};

export const lessons = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
